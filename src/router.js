import Vue from 'vue'
import Router from 'vue-router'
import List from './view/list.vue'
import Bookmark from './view/bookmark.vue'

Vue.use(Router)
const router = new Router({
	routes: [
		{
			path: '/',
			name: 'List',
            component: List,
		},
        {
            path: '/bookmark',
            name: 'Bookmark',
            component: Bookmark,
        }
    ]
})

export default router


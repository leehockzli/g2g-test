import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        bookmark: JSON.parse(sessionStorage.getItem("bookmark_list")) || []
    },

    mutations: {
        setBookmark(state, arr) {
            console.log('hey yo');
            state.bookmark = arr;
            // sessionStorage.setItem("temp", arr);
            sessionStorage.setItem("bookmark_list", JSON.stringify(arr));
        },
    },

    actions: {

    }
})